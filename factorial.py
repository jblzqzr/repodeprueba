for i in range(1,11):
    # Podriamos usar la función factorial de math pero lo haremos manualmente.
    fact = 1
    for j in range(1,i+1):
        fact = fact * j
    print("El factorial de {} es {}".format(i,fact))